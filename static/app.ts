"use strict";

var conn: WebSocket | null = null;
var cos: boolean = true;

$(() => {
    enum FlagOps {
        CLEAR_ON_SIGNAL = 1
    }

    function html(v: string): Document {
        return new DOMParser().parseFromString(v, "text/html");
    }

    // Assumes that the parsed html has a single root node,
    // and extracts that from the Document (html_*body single node* -> html_bsn)
    function html_bsn(v: string): HTMLElement {
        return html(v).body.children[0] as HTMLElement;
    }

    function basicSanitize(i: string): string {
        return i
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;");
    }

    function connect() {
        var vlen = window.location.pathname.length;
        var wsUri =
            ((window.location.protocol == "https:" && "wss://") || "ws://") +
            window.location.host +
            window.location.pathname.substr(0, vlen - "/chat.html".length) +
            "/ws/";
        conn = new WebSocket(wsUri);
        conn.onopen = () => {
            if (conn == null) return;

            // Trick the type system into forcing a string
            var chan = $("#channel-inp").val() + "";
            var tok = $("#token-inp").val() + "";

            if (chan.length < 1 || tok.length < 1) {
                return;
            }

            conn.send(
                JSON.stringify({
                    auth: {
                        channel: chan,
                        token: tok
                    }
                })
            );
            send_cos(cos);
            update_ui();
        };
        conn.onmessage = m => {
            let j = JSON.parse(m.data);

            if (j.hasOwnProperty("pin")) {
                addmessage(
                    j["pin"]["id"],
                    j["pin"]["user"],
                    j["pin"]["pinner"],
                    j["pin"]["message"]
                );
            } else if (j.hasOwnProperty("clear")) {
                clear_message(j["clear"]["target"]);
            } else {
                console.error("unknown op", j);
            }
        };
        conn.onclose = () => {
            conn = null;
            update_ui();
        };
    }

    function clear_message(target: string) {
        let pin = $("#pinlist");
        if (target == "top") {
            if (pin.children.length > 1) {
                pin.children(":first").remove();
            }
        } else if (target == "all") {
            pin.html("");
        } else {
            console.log("Unknown clear target");
        }
    }

    function addmessage(
        id: string,
        user: string,
        pinner: string,
        message: string
    ) {
        if (id == null || user == null || message == null) {
            return;
        }

        let root = html_bsn(
            `<div class="flash item3">
                <div class="is-divider"></div>
                <div class="columns">
                    <div class="column is-half">
                        <div class="columns is-mobile">
                            <div class="column is-half is-narrow"><small class="light">ID: ${basicSanitize(
                                id
                            )}</small></div>
                            <div class="column is-half is-narrow"><small class="light">User: ${basicSanitize(
                                user
                            )}</small></div>
                        </div>
                    </div>
                    <div class="column is-half">
                        <div class="columns is-mobile">
                            <div class="column is-half is-narrow"><small class="light">Pinned by: ${basicSanitize(
                                pinner
                            )}</small></div>
                            <div class="column is-half is-narrow">
                                <button class="button is-small is-round ident-dismiss">Dismiss</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="column is-full">
                        ${basicSanitize(message)}
                    </div>
                </div>
            </div>`
        );
        var ident = root.querySelector(".ident-dismiss");
        if (ident == null) {
            console.log("Creation failed");
            console.log(root);
            return;
        }

        ident.addEventListener("click", e => {
            e.preventDefault();
            root.remove();
        });

        $("#pinlist").append(root);
    }

    function disconnect() {
        if (conn == null) return;
        conn.close();
        conn = null;
        update_ui();
    }

    function update_ui() {
        var status = $("#status-tag");
        var connect = $("#connect");
        $("#op-cos").prop("checked", cos);
        if (conn == null) {
            status.removeClass("is-success");
            status.addClass("is-danger");
            status.text("Disconnected");
            connect.html("Connect...");
        } else {
            status.removeClass("is-danger");
            status.addClass("is-success");
            status.text("Connected");
            connect.html("Disconnect...");
        }
    }

    function send_cos(cos: boolean) {
        if (conn == null) {
            return;
        }
        conn.send(
            JSON.stringify({
                "flag": {
                    "flag": FlagOps.CLEAR_ON_SIGNAL,
                    "set": ($("#op-cos").prop("checked") ? true : false)
                }
            })
        )
    }

    $("#connect").click(e => {
        e.preventDefault();
        if (conn == null) {
            connect();
        } else {
            disconnect();
        }
        update_ui();
    });

    $("#clear-all").click(e => {
        e.preventDefault();
        $("#pinlist").html("");
    });

    $("#op-cos").on("change", () => {
        cos = $("#op-cos").prop("checked") ? true : false;
        send_cos(cos);
    });

    $("#op-cos").prop("checked", cos);
});
