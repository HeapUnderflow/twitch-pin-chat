#!/usr/bin/sh

# Requires ripgrep (rq), Typescript (tsc) and p7zip (7z) as deps

BUILD_CHROME="build.chrome.7z"
BUILD_FIREFOX="build.firefox.7z"

if [ ! -d "build" ]; then
    mkdir build
else 
    rm -R build
    mkdir build
fi

echo "Copying data..."
cp -Rf extension/chrome build/chrome
cp -Rf extension/firefox build/firefox

tsc --build build/chrome/tsconfig.json
tsc --build build/firefox/tsconfig.json

echo "Pruning unwated files"
fd -I ".*\.ts$" build/ -x rm '{}'
fd -I "tsconfig\.json$" build/ -x rm '{}'

echo "Compressing"

[ -f "$BUILD_CHROME"  ] && rm "$BUILD_CHROME"
[ -f "$BUILD_FIREFOX" ] && rm "$BUILD_FIREFOX"

7z a -t7z -m0=lzma2 -mx=9 -mfb=64 -md=32m -ms=on -bb3 "$BUILD_CHROME"  build/chrome
7z a -t7z -m0=lzma2 -mx=9 -mfb=64 -md=32m -ms=on -bb3 "$BUILD_FIREFOX" build/firefox

rm -R build