# Make this project

TSC = tsc
TSC_ARGS = --verbose
CARGO = cargo

CARGO_MANIFEST = ./Cargo.toml

default: server static extensions

extensions: extension_chrome extension_firefox

extension_chrome: extension/chrome/tsconfig.json
	${TSC} --build extension/chrome/tsconfig.json ${TSC_ARGS}

extension_firefox: extension/firefox/tsconfig.json
	${TSC} --build extension/firefox/tsconfig.json ${TSC_ARGS}

server: Cargo.toml
	${CARGO} build --release --manifest-path ${CARGO_MANIFEST}

static: static/tsconfig.json
	${TSC} --build static/tsconfig.json ${TSC_ARGS}

clean:
	${RM} extension/chrome/*.js
	${RM} static/*.js
	${CARGO} clean --manifest-path ${CARGO_MANIFEST}

package: default
	./pack.sh

.PHONY: extensions static