use actix::prelude::*;
use std::{
    collections::HashMap,
    fs::File,
    io::{self, BufRead, BufReader},
};

#[derive(Debug)]
pub struct Authenticate {
    pub channel: String,
    pub token:   String,
}

impl Message for Authenticate {
    type Result = bool;
}

#[derive(Debug)]
pub struct Auth {
    tokens: HashMap<String, Vec<String>>,
}

impl Actor for Auth {
    type Context = Context<Self>;
}

impl Handler<Authenticate> for Auth {
    type Result = bool;

    fn handle(&mut self, msg: Authenticate, _: &mut Self::Context) -> Self::Result {
        dbg_trace!(&msg);
        match self.tokens.get(&msg.channel) {
            Some(chn) => chn.contains(&msg.token),
            None => false,
        }
    }
}

impl Auth {
    pub fn file_source(f: &str) -> io::Result<Auth> {
        let f = BufReader::new(File::open(f)?);

        let mut v = HashMap::new();
        for line in f.lines() {
            let line = line?;
            let split = line.splitn(2, ' ').collect::<Vec<_>>();
            if split.len() < 2 {
                warn!("Skipping token line: {:?}", split);
                continue;
            }
            v.entry(String::from(split[0]))
                .or_insert_with(Vec::new)
                .push(String::from(split[1]));
        }

        Ok(dbg_trace!(Auth { tokens: v }, "auth init"))
    }
}
