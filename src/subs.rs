use crate::model::{Clear, Notif, Subscribe};
use actix::prelude::*;
use std::{collections::HashMap, time::Duration};

const DEAD_CONN_CHECK_INTERVAL: Duration = Duration::from_secs(60);

pub struct ChSubList {
    chan:      HashMap<String, Vec<Addr<crate::socket::Ws>>>,
    processed: Vec<String>,
}

impl Actor for ChSubList {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        ctx.run_interval(DEAD_CONN_CHECK_INTERVAL, |act, _| {
            trace!("DEADCONN and ID cleanup");
            if act.processed.len() >= 150 {
                act.processed = Vec::from(&act.processed[50..]);
            }

            for (_, v) in act.chan.iter_mut() {
                v.retain(Addr::connected);
            }

            act.chan.retain(|_, v| !v.is_empty());
        });
    }
}

impl Handler<Notif> for ChSubList {
    type Result = bool;

    fn handle(&mut self, msg: Notif, _: &mut Self::Context) -> bool {
        dbg_trace!(&msg);
        // if self.processed.contains(&msg.id) {
        //     trace!(">> ALREADY PROCESSED");
        //     return false;
        // }

        self.processed.push(msg.id.clone());

        if self.chan.contains_key(&msg.channel) {
            // We are only actually processing the event, if we MAY have listeners for it
            // otherwise we will just mark the message as "used"
            for sub in &self.chan[&msg.channel] {
                sub.do_send(msg.clone());
            }
        } else {
            trace!("SKIP => (nochannel[{}])", &msg.channel);
        }

        true
    }
}

impl Handler<Clear> for ChSubList {
    type Result = ();

    fn handle(&mut self, msg: Clear, _: &mut Self::Context) {
        dbg_trace!(&msg);

        if self.chan.contains_key(&msg.channel) {
            for sub in &self.chan[&msg.channel] {
                sub.do_send(msg.clone());
            }
        } else {
            trace!("SKIP => (nochannel[{}])", &msg.channel);
        }
    }
}

impl Handler<Subscribe> for ChSubList {
    type Result = ();

    fn handle(&mut self, msg: Subscribe, _: &mut Self::Context) {
        dbg_trace!(&msg.channel, "SUB_ADD");
        self.chan
            .entry(msg.channel)
            .or_insert_with(Vec::default)
            .push(msg.addr);

        dbg_trace!(self.chan.keys().collect::<Vec<_>>());
    }
}

impl ChSubList {
    pub fn new() -> ChSubList {
        ChSubList {
            chan:      HashMap::new(),
            processed: Vec::new(),
        }
    }
}
