use crate::{
    auth,
    model::{Clear, Notif, Op, WsFlags},
    subs,
};
use actix::prelude::*;
use actix_web_actors::ws;
use std::time::{Duration, Instant};

/// How often heartbeat pings are sent
const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
/// How long before lack of client response causes a timeout
const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);

/// websocket connection is long running connection, it easier
/// to handle with an actor
pub struct Ws {
    /// Client must send ping at least once per 10 seconds (CLIENT_TIMEOUT),
    /// otherwise we drop connection.
    hb: Instant,
    auth: Addr<auth::Auth>,
    subs: Addr<subs::ChSubList>,
    flags: WsFlags,
    authenticated: bool,
}

impl Actor for Ws {
    type Context = ws::WebsocketContext<Self>;

    /// Method is called on actor start. We start the heartbeat process here.
    fn started(&mut self, ctx: &mut Self::Context) { self.hb(ctx); }
}

impl Handler<Notif> for Ws {
    type Result = bool;

    fn handle(&mut self, msg: Notif, ctx: &mut Self::Context) -> bool {
        match dbg_trace!(serde_json::to_string(&Op::Pin {
            channel: msg.channel,
            id:      msg.id,
            message: msg.message,
            user:    msg.pinned,
            pinner:  msg.pinner,
        })) {
            Ok(val) => ctx.text(val),
            Err(e) => ctx.text(
                serde_json::to_string(&Op::Error {
                    message: format!("{:?}", e),
                })
                .unwrap_or_else(|_| String::from("fatal internal error")),
            ),
        }

        true
    }
}

impl Handler<Clear> for Ws {
    type Result = ();

    fn handle(&mut self, msg: Clear, ctx: &mut Self::Context) {
        if !self.flags.contains(WsFlags::CLEAR_ON_SIGNAL) {
            trace!("CLEAR_ON_SIGNAL off");
            return;
        }

        match dbg_trace!(serde_json::to_string(&Op::Clear {
            channel: msg.channel,
            user:    msg.user,
            target:  msg.target,
        })) {
            Ok(val) => ctx.text(val),
            Err(e) => ctx.text(
                serde_json::to_string(&Op::Error {
                    message: format!("{:?}", e),
                })
                .unwrap_or_else(|_| String::from("fatal internal error")),
            ),
        }
    }
}

/// Handler for `ws::Message`
impl StreamHandler<ws::Message, ws::ProtocolError> for Ws {
    fn handle(&mut self, msg: ws::Message, ctx: &mut Self::Context) {
        // process websocket messages
        trace!("WS: {:?}", msg);
        match msg {
            ws::Message::Ping(msg) => {
                self.hb = Instant::now();
                ctx.pong(&msg);
            },
            ws::Message::Pong(_) => {
                self.hb = Instant::now();
            },
            ws::Message::Text(text) => {
                let m = match serde_json::from_str(&text) {
                    Ok(data) => data,
                    Err(ref e) if self.authenticated => {
                        ctx.text(
                            serde_json::to_string(&Op::Error {
                                message: format!("{:?}", e),
                            })
                            .unwrap_or_else(|_| {
                                String::from("fatal error occured internally, panic!")
                            }),
                        );
                        return;
                    },
                    Err(_) => {
                        ctx.stop();
                        return;
                    },
                };

                match m {
                    Op::Auth { channel, token } => self
                        .auth
                        .send(auth::Authenticate {
                            channel: channel.clone(),
                            token,
                        })
                        .into_actor(self)
                        .then(|res, act, ctx| {
                            match res {
                                Ok(v) => {
                                    if dbg_trace!(v) {
                                        act.authenticated = true;
                                        act.subs.do_send(crate::model::Subscribe {
                                            channel,
                                            addr: ctx.address(),
                                        });
                                    } else {
                                        warn!("authentication failed");
                                        act.authenticated = false;
                                        ctx.stop();
                                    }
                                },
                                Err(e) => {
                                    warn!("Authentication errored {:?}", e);
                                    ctx.stop();
                                },
                            }
                            fut::ok(())
                        })
                        .wait(ctx),
                    Op::Flag { flag, set } => {
                        let flags = dbg_trace!(WsFlags::from_bits_truncate(flag), "FLAGS");
                        dbg_trace!(set, "FLAGS->SET");

                        if flags.contains(WsFlags::CLEAR_ON_SIGNAL) {
                            self.flags.set(WsFlags::CLEAR_ON_SIGNAL, set);
                        }
                    },
                    _ => (),
                }
            },
            ws::Message::Binary(_) => {
                warn!("Encountered binary data: exiting...");
                ctx.stop();
            },
            ws::Message::Close(_) => {
                debug!("Connection closed: exiting...");
                ctx.stop();
            },
            ws::Message::Nop => (),
        }
    }
}

impl Ws {
    pub fn new(auth: Addr<auth::Auth>, subs: Addr<subs::ChSubList>) -> Self {
        Self {
            hb: Instant::now(),
            auth,
            subs,
            flags: WsFlags::default(),
            authenticated: false,
        }
    }

    /// helper method that sends ping to client every second.
    ///
    /// also this method checks heartbeats from client
    fn hb(&self, ctx: &mut <Self as Actor>::Context) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            // check client heartbeats
            if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
                // heartbeat timed out
                warn!("Websocket Client heartbeat failed, disconnecting!");

                // stop actor
                ctx.stop();

                // don't try to send a ping
                return;
            }

            if !act.authenticated {
                warn!("client didnt authenticate after 5 seconds, exiting");
                ctx.stop();
                return;
            }

            ctx.ping("");
        });
    }
}
