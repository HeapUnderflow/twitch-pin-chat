//! Server side for the twitch pin extension

#[macro_use]
extern crate log;

#[macro_use]
mod macros;
mod auth;
mod model;
mod socket;
mod subs;

use crate::model::ClearTarget;
use actix::prelude::*;
use actix_files as fs;
use actix_web::{middleware, web, App, Error, HttpRequest, HttpResponse, HttpServer};
use actix_web_actors::ws;
use futures::future::{Either, IntoFuture};
use serde::Deserialize;
use serde_json::json;
use socket::Ws;
use std::io;

struct State {
    auth: Addr<auth::Auth>,
    subs: Addr<subs::ChSubList>,
}

#[derive(Debug, Clone, Deserialize)]
struct DoPin {
    token:        String,
    channel:      String,
    id:           String,
    pinned_user:  String,
    pinning_user: String,
    message:      String,
}

/// do websocket handshake and start `Ws` actor
fn ws_index(
    state: web::Data<State>,
    r: HttpRequest,
    stream: web::Payload,
) -> Result<HttpResponse, Error> {
    dbg_trace!(ws::start(
        Ws::new(state.auth.clone(), state.subs.clone()),
        &dbg_trace!(r),
        stream
    ))
}

fn pin(
    state: web::Data<State>,
    data: web::Json<DoPin>,
) -> impl Future<Item = HttpResponse, Error = Error> {
    dbg_trace!(&data);
    let subs = state.subs.clone();
    state
        .auth
        .send(auth::Authenticate {
            token:   data.token.clone(),
            channel: data.channel.clone(),
        })
        .from_err()
        .and_then(move |valid| {
            if dbg_trace!(valid) {
                Either::A(
                    subs.send(dbg_trace!(crate::model::Notif {
                        id:      ammonia::clean(&data.id),
                        channel: ammonia::clean(&data.channel),
                        message: ammonia::clean(&data.message),
                        pinned:  ammonia::clean(&data.pinned_user),
                        pinner:  ammonia::clean(&data.pinning_user),
                    }))
                    .from_err()
                    .and_then(|v| {
                        HttpResponse::Ok()
                            .json(json!({ "result": v }))
                            .into_future()
                    })
                    .or_else(|_| HttpResponse::BadRequest().finish().into_future()),
                )

            // HttpResponse::Ok().finish()
            } else {
                Either::B(HttpResponse::Unauthorized().finish().into_future())
            }
        })
        .or_else(|_| HttpResponse::InternalServerError().finish().into_future())
}

fn clear_msg(
    state: web::Data<State>,
    req: HttpRequest,
) -> impl Future<Item = HttpResponse, Error = Error> {
    let chn = match req.match_info().get("channel") {
        Some(c) => c.to_owned(),
        None => return Either::A(HttpResponse::BadRequest().finish().into_future()),
    };

    let tgt = match req.match_info().get("target") {
        Some(t) => t,
        None => return Either::A(HttpResponse::BadRequest().finish().into_future()),
    };

    let target = match tgt {
        "top" => ClearTarget::Top,
        "all" => ClearTarget::All,
        _ => return Either::A(HttpResponse::BadRequest().finish().into_future()),
    };

    let raw_token = match req.headers().get("x-api-key") {
        Some(t) => t,
        None => return Either::A(HttpResponse::Unauthorized().finish().into_future()),
    };

    let token = match raw_token.to_str() {
        Ok(s) => s.to_owned(),
        Err(_) => {
            return Either::A(
                HttpResponse::BadRequest()
                    .body("invalid bytes in data")
                    .into_future(),
            )
        },
    };

    let subs = state.subs.clone();
    let ch = chn.to_owned();
    Either::B(
        state
            .auth
            .send(auth::Authenticate { token, channel: ch })
            .from_err()
            .and_then(move |valid| {
                if dbg_trace!(valid) {
                    Either::A(
                        subs.send(dbg_trace!(crate::model::Clear {
                            channel: ammonia::clean(&chn),
                            user: "example_user".to_owned(),
                            target
                        }))
                        .from_err()
                        .and_then(|_| HttpResponse::Ok().finish().into_future())
                        .or_else(|_| HttpResponse::BadRequest().finish().into_future()),
                    )
                } else {
                    Either::B(HttpResponse::Unauthorized().finish().into_future())
                }
            })
            .or_else(|_| HttpResponse::InternalServerError().finish().into_future()),
    )
}

fn main() -> std::io::Result<()> {
    if let Err(e) = kankyo::load() {
        match e.kind() {
            io::ErrorKind::NotFound => (),
            _ => panic!(e),
        }
    }

    env_logger::init();

    let sys = System::new("web");

    let auth = auth::Auth::file_source("tokens.txt")?.start();
    let subs = subs::ChSubList::new().start();

    HttpServer::new(move || {
        App::new()
            .data(State {
                auth: auth.clone(),
                subs: subs.clone(),
            })
            // enable logger
            .wrap(middleware::Logger::default())
            // websocket route
            .service(web::resource("/ws/").route(web::get().to(ws_index)))
            // post route
            .service(web::resource("/ws/post").route(web::post().to_async(pin)))
            // delete topmost
            .service(
                web::resource("/ws/delete/{channel}/{target}")
                    .route(web::get().to_async(clear_msg)),
            )
            // static files
            .service(fs::Files::new("/", "static/").index_file("index.html"))
    })
    // start http server on 127.0.0.1:8080
    .bind(&kankyo::key("BIND").unwrap_or_else(|| String::from("127.0.0.1:8080")))?
    .start();

    sys.run()
}
