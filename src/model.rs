//! Model definitions for the various program parts

use actix::prelude::*;
use bitflags::bitflags;
use serde::{Deserialize, Serialize};

/// Target for the "Clear" operation
#[derive(Debug, Serialize, Deserialize, Clone, Copy)]
pub enum ClearTarget {
    #[serde(rename = "top")]
    Top,
    #[serde(rename = "all")]
    All,
}

/// Op send over the websocket connection
#[derive(Debug, Serialize, Deserialize)]
pub enum Op {
    #[serde(rename = "auth", skip_serializing)]
    Auth { token: String, channel: String },
    #[serde(rename = "flag", skip_serializing)]
    Flag { flag: u8, set: bool },
    #[serde(rename = "pin")]
    Pin {
        channel: String,
        id:      String,
        message: String,
        user:    String,
        pinner:  String,
    },
    #[serde(rename = "clear")]
    Clear {
        channel: String,
        user:    String,
        target:  ClearTarget,
    },
    #[serde(rename = "error", skip_deserializing)]
    Error { message: String },
}

#[derive(Debug, Clone)]
pub struct Notif {
    pub id:      String,
    pub channel: String,
    pub message: String,
    pub pinner:  String,
    pub pinned:  String,
}

impl Message for Notif {
    type Result = bool;
}

#[derive(Debug, Clone)]
pub struct Clear {
    pub channel: String,
    pub user:    String,
    pub target:  ClearTarget,
}

impl Message for Clear {
    type Result = ();
}

#[derive(Clone)]
pub struct Subscribe {
    pub channel: String,
    pub addr:    Addr<crate::socket::Ws>,
}

impl Message for Subscribe {
    type Result = ();
}

bitflags! {
    #[derive(Default, Serialize, Deserialize)]
    pub struct WsFlags: u8 {
        const CLEAR_ON_SIGNAL = 1;
    }
}
