#![allow(unused_macros)]
/// Behaves similar to the [`dbg`] macro, but instead uses [`trace`] instead of stdout.
/// Additionally it omits the expression in favor of file location info
macro_rules! dbg_trace {
    ($e:expr) => {{
        #[allow(clippy::all)]
        match $e {
            tmp => {
                trace!("[{}#{}:{}] = {:?}", file!(), line!(), column!(), tmp);
                tmp
            },
        }
    }};

    ($e:expr, $m:expr) => {{
        #[allow(clippy::all)]
        match $e {
            tmp => {
                trace!(
                    "[{}#{}:{}] {{ {} }} = {:?}",
                    file!(),
                    line!(),
                    column!(),
                    $m,
                    tmp
                );
                tmp
            },
        }
    }};

    (PRETTY; $e:expr) => {{
        {
            #[allow(clippy::all)]
            match $e {
                tmp => {
                    let f = format!("{:#?}", tmp);

                    for l in f.split('\n') {
                        trace!("[{}#{}:{}] = {}", file!(), line!(), column!(), l);
                    }

                    tmp
                },
            }
        }
    }};

    (PRETTY; $e:expr, $m:expr) => {{
        {
            #[allow(clippy::all)]
            match $e {
                tmp => {
                    let f = format!("{:#?}", tmp);

                    for l in f.split('\n') {
                        trace!(
                            "[{}#{}:{}] {{ {} }} = {}",
                            file!(),
                            line!(),
                            column!(),
                            $m,
                            l
                        );
                    }

                    tmp
                },
            }
        }
    }};
}

/// Log an [`error`] if the given expression returns an Err
macro_rules! err_log {
    ($e:expr) => {
        if let Err(why) = $e {
            error!(
                "[{}#{}:{}] {{ {} }} = {:?}",
                file!(),
                line!(),
                column!(),
                stringify!($e),
                why
            );
        }
    };
}
