#!/usr/bin/fish

echo "PWD="(pwd)

type web-ext >/dev/null
if test $status -ne 0
    echo "Missing web-ext"
    exit 1
end

echo "Cleaning up previous builds..."
if test -d "build/"
    rm -Rf build/*
else
    mkdir build
end

cp -Rf firefox/* build/

echo "Building..."
tsc --build build/tsconfig.json

if test $status -ne 0
    exit 1
end

echo "Removing unwanted artifacts"
fd -I '.*\.ts$' build/ -x rm '{}'
fd -I 'tsconfig\.json$' build/ -x rm '{}'

pushd build

# echo "Building ext..."
# web-ext build

# if test $status -ne 0
#     exit 1
# end

echo "Loading creds..."
set -lx WEB_EXT_API_KEY (head -1 ~/.creds/firefox-signing.txt)
set -lx WEB_EXT_API_SECRET (head -2 ~/.creds/firefox-signing.txt | tail -1)

echo "K="$WEB_EXT_API_KEY
echo "S="$WEB_EXT_API_SECRET

echo "Signing ext..."
web-ext sign

set -e WEB_EXT_API_KEY
set -e WEB_EXT_API_SECRET

popd

echo "Done!"