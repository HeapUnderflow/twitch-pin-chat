# TODO's

- [ ] Make URL Customizable
- [ ] Remove all instances of hardcoded URL's
  - [ ] Manifest
  - [ ] Make URL Clickable in Ext-Icon
- [ ] Add "Pinned By"
  - [ ] Extension
  - [ ] Server
  - [ ] Page
- [x] Autologin on ChatPage
