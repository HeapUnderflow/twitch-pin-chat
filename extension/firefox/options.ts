var url = document.getElementById("url") as HTMLInputElement;
var username = document.getElementById("username") as HTMLInputElement;
var addauth = document.getElementById("addauth") as HTMLElement;
var creds = document.getElementById("creds") as HTMLElement;
var save = document.getElementById("save") as HTMLElement;

function createCred(at: Node, chan?: string, tok?: string, autologin?: boolean) {
    var div = document.createElement("div");
    var labl_chan = document.createElement("label");
    var labl_tok = document.createElement("label");
    var labl_auto = document.createElement("label");
    var inp_chan = document.createElement("input");
    var inp_tok = document.createElement("input");
    var inp_auto = document.createElement("input");
    var close = document.createElement("button");

    inp_chan.type = "text";
    inp_chan.placeholder = "Channel";
    if (chan != null) {
        inp_chan.value = chan;
    }

    inp_tok.type = "text";
    inp_tok.placeholder = "Token";
    if (tok != null) {
        inp_tok.value = tok;
    }

    inp_auto.type = "checkbox";
    inp_auto.checked = autologin || false;
    inp_auto.innerText = "Autologin?";

    labl_chan.append("Channel: ", inp_chan);
    labl_tok.append("Token: ", inp_tok);
    labl_auto.append("Autologin? ", inp_auto);

    close.innerText = "X";
    close.onclick = e => {
        e.preventDefault();
        div.remove();
    };

    div.append(labl_chan, labl_tok, labl_auto, close);

    at.appendChild(div);
}

chrome.storage.sync.get(["username", "url", "auth"], function(data) {
    if (data.url && data.username) {
        url.value = data.url;
        username.value = data.username;
    }

    addauth.onclick = e => {
        e.preventDefault();
        createCred(creds);
    };

    save.onclick = e => {
        e.preventDefault();
        var valid = true;

        for (let idx = 0; idx < creds.children.length; idx++) {
            const cr = creds.children[idx];
            const crChan = cr.children[0].children[0] as HTMLInputElement;
            const crToken = cr.children[1].children[0] as HTMLInputElement;
            if (crChan.value.length < 1) {
                crChan.style.border = "2px solid red";
                crChan.style.borderRadius = "4px";
                valid = false;
            } else {
                crChan.style.border = null;
                crChan.style.borderRadius = null;
            }
            if (crToken.value.length < 1) {
                crToken.style.border = "2px solid red";
                crToken.style.borderRadius = "4px";
                valid = false;
            } else {
                crToken.style.border = null;
                crToken.style.borderRadius = null;
            }
        }

        if (!valid) {
            return;
        }

        var stor_creds = [];
        for (let idx = 0; idx < creds.children.length; idx++) {
            const cr = creds.children[idx];

            var chn = (cr.children[0].children[0] as HTMLInputElement).value;
            var tok = (cr.children[1].children[0] as HTMLInputElement).value;
            var auto = (cr.children[2].children[0] as HTMLInputElement).checked;

            stor_creds.push({
                channel: chn,
                token: tok,
                autologin: auto
            });
        }

        chrome.storage.sync.set(
            {
                url: url.value,
                username: username.value,
                auth: stor_creds
            },
            () => {
                alert("Saved");
            }
        );
    };

    if (data.auth) {
        for (let idx = 0; idx < data.auth.length; idx++) {
            const cred = data.auth[idx];
            createCred(creds, cred.channel, cred.token, cred.autologin);
        }
    }
});
