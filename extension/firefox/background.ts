chrome.runtime.onInstalled.addListener(() => {
    chrome.storage.sync.get(["url", "auth", "username"], (data) => {

        var set = {};
        if (!data.hasOwnProperty("url")) {
            data.url = "https://thesinglebyte.com/twitch/eggly/ws";
        }
        if (!data.hasOwnProperty("auth")) {
            data.auth = [];
        }
        if (!data.hasOwnProperty("username")) {
            data.username = "";
        }

        chrome.storage.sync.set(set,
            () => {
                console.log("Configuration set");
                console.log(set);
            }
        );
    });

    chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
        if (tab.url === undefined) {
            return;
        }
        
        var m = tab.url.match(/https:\/\/(www\.)?twitch\.tv\/[a-zA-Z0-9_]+/g);
        if (m) {
            console.log("pagact=show");
            chrome.pageAction.show(tabId);
        } else {
            console.log("pagact=hide");
            chrome.pageAction.hide(tabId);
        }
    });
});

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    chrome.storage.sync.get(["auth", "url", "username"], data => {
        console.log(request);
        for (let i = 0; i < data.auth.length; i++) {
            const au = data.auth[i];

            if (au.channel == request.channel) {
                request.token = au.token;
                console.log("token found");
                var xhr = new XMLHttpRequest();
                xhr.onload = v => {
                    console.log("ST = " + xhr.status);
                    if (xhr.status == 200) {
                        let j = JSON.parse(xhr.response);
                        sendResponse({
                            br: false,
                            ua: false,
                            re: j.result
                        });
                    } else if (xhr.status == 401) {
                        sendResponse({
                            br: false,
                            ua: true,
                            re: null
                        });
                    } else if (xhr.status == 400) {
                        sendResponse({
                            br: true,
                            ua: false,
                            re: null
                        });
                    } else {
                        sendResponse({
                            br: false,
                            ua: false,
                            re: null
                        });
                    }
                };
                xhr.open("POST", data.url + "/post");
                xhr.setRequestHeader("Content-Type", "application/json");
                console.log("[" + data.url + "/post] => " + JSON.stringify(request));
                xhr.send(JSON.stringify(request));
                return;
            }
        }
    });
    return true;
});
