chrome.runtime.onInstalled.addListener(() => {
    chrome.storage.sync.get(["url", "auth", "username"], (data) => {

        var set = {};
        if (!data.hasOwnProperty("url")) {
            data.url = "https://thesinglebyte.com/twitch/eggly/ws";
        }
        if (!data.hasOwnProperty("auth")) {
            data.auth = [];
        }
        if (!data.hasOwnProperty("username")) {
            data.username = "";
        }

        chrome.storage.sync.set(set,
            () => {
                console.log("Configuration set");
                console.log(set);
            }
        );
    });

    chrome.declarativeContent.onPageChanged.removeRules(undefined, () => {
        chrome.declarativeContent.onPageChanged.addRules([
            {
                conditions: [
                    new chrome.declarativeContent.PageStateMatcher({
                        pageUrl: {
                            hostEquals: "twitch.tv"
                        }
                    }),
                    new chrome.declarativeContent.PageStateMatcher({
                        pageUrl: {
                            hostEquals: "www.twitch.tv"
                        }
                    })
                ],
                actions: [new chrome.declarativeContent.ShowPageAction()]
            }
        ]);
    });
});

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    chrome.storage.sync.get(["auth", "url", "username"], data => {
        console.log(request);
        for (let i = 0; i < data.auth.length; i++) {
            const au = data.auth[i];

            if (au.channel == request.channel) {
                request.token = au.token;
                console.log("token found");
                var xhr = new XMLHttpRequest();
                xhr.onload = v => {
                    console.log("ST = " + xhr.status);
                    if (xhr.status == 200) {
                        let j = JSON.parse(xhr.response);
                        sendResponse({
                            br: false,
                            ua: false,
                            re: j.result
                        });
                    } else if (xhr.status == 401) {
                        sendResponse({
                            br: false,
                            ua: true,
                            re: null
                        });
                    } else if (xhr.status == 400) {
                        sendResponse({
                            br: true,
                            ua: false,
                            re: null
                        });
                    } else {
                        sendResponse({
                            br: false,
                            ua: false,
                            re: null
                        });
                    }
                };
                xhr.open("POST", data.url + "/post");
                xhr.setRequestHeader("Content-Type", "application/json");
                console.log(JSON.stringify(request));
                xhr.send(JSON.stringify(request));
                return;
            }
        }
    });
    return true;
});
