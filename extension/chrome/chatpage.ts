(() => {
    chrome.storage.sync.get(["auth"], storage => {
        if (storage.auth.length < 1) {
            return;
        }

        for (let idx = 0; idx < storage.auth.length; idx++) {
            const a = storage.auth[idx];
            if (a.autologin) {
                const chan = document.getElementById("channel-inp");
                const token = document.getElementById("token-inp");

                if (chan != null || token != null) {
                    (chan as HTMLInputElement).value = a.channel;
                    (token as HTMLInputElement).value = a.token;
                }
                break;
            }
        }
    });
})();
