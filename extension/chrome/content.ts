(() => {
    // i cannot assume to have jquery available here,
    // so we will have to work with purejs stuff
    const ICON =
        '<svg width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="thumbtack" class="svg-inline--fa fa-thumbtack fa-w-12" role="img" \
            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M298.028 214.267L285.793 96H328c13.255 0 24-10.745 \
            24-24V24c0-13.255-10.745-24-24-24H56C42.745 0 32 10.745 32 24v48c0 13.255 10.745 24 24 24h42.207L85.972 214.267C37.465 236.82 0 277.261 0 \
            328c0 13.255 10.745 24 24 24h136v104.007c0 1.242.289 2.467.845 3.578l24 48c2.941 5.882 11.364 5.893 14.311 0l24-48a8.008 8.008 0 0 0 \
            .845-3.578V352h136c13.255 0 24-10.745 24-24-.001-51.183-37.983-91.42-85.973-113.733z"></path></svg>';

    var CHAN = "";
    var USER = "";

    var pgname = window.location.href.match(
        /https?:\/\/.*twitch\.tv\/(?:popout\/([\w-]+)|([\w-]+)$)/
    );
    if (pgname != null) {
        CHAN = pgname[1] !== undefined ? pgname[1] : pgname[2];
    } else {
        CHAN = "// ERROR RETRIEVING CHANNEL NAME //";
    }

    function extractTwitchMessage(node: HTMLElement) {
        var container = node.querySelector(".message");
        var result = [];

        if (container == null) {
            return node.innerText;
        } else {
            for (let idx = 0; idx < container.children.length; idx++) {
                const child = container.children[idx] as HTMLElement;
                const imgChild = child.querySelector(
                    "img.chat-line__message--emote"
                );
                if (imgChild != null) {
                    result.push(imgChild.getAttribute("alt"));
                } else {
                    result.push(child.innerText);
                }
            }
        }

        return result.join("");
    }

    function createPinIcon(node: HTMLElement) {
        var user = (node.querySelector(
            "span.chat-author__display-name"
        ) as HTMLElement).innerText;
        var mesg = extractTwitchMessage(node);
        var mid = "" + Math.floor(Math.random() * 2000000000);

        var el = document.createElement("span");
        el.classList.add("fa", "fa-thumbtack");
        el.innerHTML = ICON;
        el.onclick = e => {
            e.preventDefault();
            /*
                struct DoPin {
                    token:        String, // added in bg.ts
                    channel:      String,
                    id:           String,
                    pinned_user:  String,
                    pinning_user: String,
                    message:      String,
                }
            */
            chrome.runtime.sendMessage(
                {
                    id: mid,
                    channel: CHAN,
                    pinned_user: user,
                    pinning_user: USER,
                    message: mesg
                },
                function(res) {
                    el.remove();
                    if (res.br) {
                        alert(
                            "Extension sent a malformed message, please update!"
                        );
                    } else if (res.ua) {
                        alert(
                            "You are not authorized to proceed with this action"
                        );
                    } else if (res.re == null) {
                        alert("A internal server error occured");
                    } else {
                        if (!res.re) {
                            alert("id colision");
                        }
                    }
                }
            );
            return false;
        };

        node.children[0].prepend(el);
    }

    function setup() {
        var targetNode = document.querySelector(
            "div.simplebar-scroll-content div.simplebar-content div.tw-flex-grow-1.tw-full-height.tw-pd-b-1"
        );

        if (!targetNode) {
            console.log("missing node, waiting");
            setTimeout(setup, 100);
            return;
        }

        // Delay the creation of the listener by 500ms
        //
        setTimeout(() => {
            var watchcfg = {
                attributes: true,
                childList: true,
                subtree: true
            };

            function onchange(list: MutationRecord[], obs: MutationObserver) {
                for (let idx = 0; idx < list.length; idx++) {
                    const mutation = list[idx];
                    if (
                        mutation.type === "childList" &&
                        mutation.addedNodes.length
                    ) {
                        for (
                            let idy = 0;
                            idy < mutation.addedNodes.length;
                            idy++
                        ) {
                            const node = mutation.addedNodes[
                                idy
                            ] as HTMLElement;

                            if (
                                !node.classList.contains("chat-line__message")
                            ) {
                                continue;
                            }

                            createPinIcon(node);
                        }
                    }
                }
            }

            var observer = new MutationObserver(onchange);
            observer.observe(targetNode as Node, watchcfg);
            console.log(observer);
        }, 500);
    }

    chrome.storage.sync.get(["auth", "username"], data => {
        if (!data.username || data.username == "") {
            return;
        }

        USER = data.username;

        for (let idx = 0; idx < data.auth.length; idx++) {
            const el = data.auth[idx];
            if (el.channel == CHAN) {
                setup();
                return;
            }
        }
    });
})();
